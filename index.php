<?php
include 'connect.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Personal Data</title>
</head>
<body>
<a href="login.php?action=logout">LogOut</a>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Adress</th>
            <th>Mail</th>
            <th>Phone Number</th>
            <th>Date</th>
            <th>Update</th>
            <th>Delete</th>
            <th><a href="insert.php">Insert New</a></td>
        </tr>
        <?php
            $sql = "SELECT * FROM personal_data";
            $result = $conn->query($sql);

            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    echo "<tr>
                            <td>" . $row["id"] . "</td>
                            <td>" . $row["name"] . "</td>
                            <td>" . $row["adress"] . "</td>
                            <td>" . $row["mail"] . "</td>
                            <td>" . $row["phone_number"] . "</td>
                            <td>" . $row["date"] . "</td>
                            <td>" . $row["update_date"] . "</td>
                            <td>
                                <a href='delete.php?id=".$row["id"]."'>Delete</a>
                            </td>
                        </tr>";
                }
            }
        ?>

    </table>

    
</body>
</html>